# Getting started
Based on: https://www.youtube.com/watch?v=WPs5QSCu9J8

### Building the Docker image
- Install [Docker Toolbox](https://www.docker.com/products/docker-toolbox)
- Clone this repository
- Open a terminal and change the working directory to the folder with the Dockerfile, eg:
`cd path/to/folder`
- Build the image from your terminal:
`docker build --tag kgc/unity-jenkins-master .`

### Running the Docker container
- Run a container with the image:
`docker run -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home kgc/unity-jenkins-master`
- This will make a shared volume between the Docker container with the image and the docker-machine VM that runs all the containers. This ensures that the data saved by Jenkins will be persisted across container start/stops and that it survives a container upgrade.

### Setting up the slave in Jenkins
- Mark it with label `unity`
- Set slave to have 1 executor, as Unity doesnt support multiple open projects simultaneously

### Setting up a Unity job in Jenkins
- Building the game
`-quit -batchmode -projectPath "$WORKSPACE" -buildWindowsPlayer "Artifacts/game.x86.exe" -logFile "$WORKSPACE/unity3d_editor.log"`

### Running tests for the game (recent Unity versions)
- Running edit-mode tests
`-quit -batchmode -projectPath "$WORKSPACE" -runTests -testPlatform editmode -testResults "$WORKSPACE/Artifacts/Windows/x86/test_results_editmode.xml"`
- Running play-mode tests
`-quit -batchmode -projectPath "$WORKSPACE" -runTests -testPlatform playmode -testResults "$WORKSPACE/Artifacts/Windows/x86/test_results_playmode.xml"`

### Running tests for the game (legacy Unity versions)
- Running unit tests for the game
`-quit -batchmode -projectPath "$WORKSPACE" -executeMethod UnityTest.Batch.RunUnitTests -resultFilePath="$WORKSPACE/Tests/Unit/results.xml"`

- Running integration tests for the game
`-quit -batchmode -projectPath "$WORKSPACE" -executeMethod UnityTest.Batch.RunIntegrationTests -targetPlatform=StandaloneWindows -resultsFileDirectory="$WORKSPACE/Tests/Integration/"`

### Troubleshooting

- If installing plugins during the `docker build`, then make sure that you convert the line-endings of the `plugins.txt` file to Unix. If you're on Windows, Notepad++ has a built in tool for that.
- If you need to update Jenkins, you'll need to update the underlying Jenkins image that this custom Dockerfile builds upon. First, run `docker pull jenkins/jenkins:lts` to update that underlying image. Secondly, re-build our custom image using by running `docker build --tag kgc/unity-jenkins-master .`. If you'll have to restart any running containers to have them use the new image, `docker ps` will show you the names, then run `docker kill <name>` to kill the container.
- If you need more space on the virtual machine, create a new virtual machine with as much space as you need by running `docker-machine create --driver virtualbox --virtualbox-disk-size "<kb size>" <vm name>`