# Based on the official Jenkins docker image
# See: https://github.com/jenkinsci/docker
FROM jenkins/jenkins:lts

# Explicitly define the slave port (default=50000)
ENV JENKINS_SLAVE_AGENT_PORT 50000

# Copy plugins configuration file from host into docker image
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt

# Then install Jenkins plugins from plugin configuration file (using script contained in official Jenkins image)
# Reads file line by line, installing each plugin (does not override already existing plugins)
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

# Copy over the console parsing properties from the host machine to the docker image so it can be used by Jenkins
# Used by Log Parser Plugin (Jenkins Plugin)
COPY console.parsing.unity.txt /tmp/console.parsing.unity.txt

# For mail server, see: https://github.com/tomav/docker-mailserver